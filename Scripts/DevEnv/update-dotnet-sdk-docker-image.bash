echo "Logging into GitLab... You may be prompted for credentials."
docker login registry.gitlab.com

echo "Pulling container image..."

docker pull mcr.microsoft.com/dotnet/sdk:$1

baseTag=registry.gitlab.com/2812-ac-portfolio/rxle
newTag=$baseTag/dotnet/sdk:$1
latestTag=$baseTag/dotnet/sdk:latest

echo "Pushing new tags..."

docker tag mcr.microsoft.com/dotnet/sdk:$1 $newTag
docker tag mcr.microsoft.com/dotnet/sdk:$1 $latestTag

docker push $newTag
docker push $latestTag